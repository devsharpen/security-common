<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Event;

class DefaultAuthenticationHandler implements AuthenticationHandler
{

    /**
     * @var callable
     */
    private $listener;

    /**
     * DefaultAuthenticationHandler constructor.
     *
     * @param callable $listener
     */
    public function __construct(callable $listener)
    {
        $this->listener = $listener;
    }

    public function getAuthenticationListener(): callable
    {
        return $this->listener;
    }
}