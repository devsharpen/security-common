<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Event;

interface AuthenticationHandler
{
    public function getAuthenticationListener(): callable;
}