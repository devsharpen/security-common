<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Event;

interface AuthenticationEvent
{
    public function getName(): string;

    public function getTarget();

    public function getAttributes(): array;

    public function getAttribute(string $key, $default = null);

    public function setAttribute(string $key, $value): void;

    public function setName(string $name): void;

    public function setTarget($target): void;

    public function setAttributes($attributes): void;

    public function stopPropagation(bool $flag = true): void;

    public function propagationIsStopped(): bool;
}