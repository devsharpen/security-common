<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Event;

use Illuminate\Support\Collection;

class AuthenticationDispatcher implements AuthenticationEmitter
{
    /**
     * @var \Illuminate\Support\Collection
     */
    private $events;

    /**
     * @var array
     */
    private $availableEvents = [];

    /**
     * Dispatcher constructor.
     *
     * @param array $availableEvents
     */
    public function __construct(array $availableEvents = [])
    {
        $this->availableEvents = $availableEvents; // assert all string
        $this->events = new Collection;
    }

    public function newEvent(string $name = null, $target = null, $params = null): AuthenticationEvent
    {
        return new DefaultAuthenticationEvent($name ?? 'default_event', $target, $params);
    }

    public function dispatch(AuthenticationEvent $event): void
    {
        $this->fire($event);
    }

    public function until(AuthenticationEvent $event, callable $callback): void
    {
        $this->fire($event, $callback);
    }

    public function attach(string $event, callable $listener, int $priority = 1): AuthenticationHandler
    {
        if (!empty($this->availableEvents) && !in_array($event, $this->availableEvents, true)) {
            throw new \InvalidArgumentException('Unknown event name given: $event');
        }

        $handler = new DefaultAuthenticationHandler($listener);

        $this->pushNewEvent($event, $priority, $handler);

        return $handler;
    }

    public function detach(AuthenticationHandler $handler): bool
    {
        $events = $this->events->reject(function (Event $event) use ($handler) {
            return $event->equalsHandler($handler);
        });

        if ($events->count() !== $this->events->count()) {
            $this->events = $events;

            return true;
        }

        return false;
    }

    private function fire(AuthenticationEvent $action, callable $callback = null)
    {
        $this->getListeners($action)->each(function (Event $event) use ($action, $callback) {

            $listener = $event->handler->getAuthenticationListener();
            $listener($action);

            if ($action->propagationIsStopped()) {
                return false;
            }

            if (null !== $callback && null !== $callback($action)) {
                return false;
            }
        });
    }

    private function getListeners(AuthenticationEvent $action): Collection
    {
        return $this->events->filter(function (Event $event) use ($action) {
            return $event->equalsName($action);
        })->sortByDesc('priority', SORT_NUMERIC)->flatten();
    }

    private function pushNewEvent(string $name, int $priority, AuthenticationHandler $handler): void
    {
        $this->events->push(new Event($name, $priority, $handler));
    }
}