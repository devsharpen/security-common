<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Event;

interface AuthenticationListener
{
    public function __invoke(AuthenticationEvent $event);
}