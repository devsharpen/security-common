<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Event;

class Event
{

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $priority;

    /**
     * @var \Devsharpen\Security\Common\Event\AuthenticationHandler
     */
    public $handler;

    public function __construct(string $name, int $priority, AuthenticationHandler $handler)
    {
        $this->name = $name;
        $this->priority = $priority . '.0';
        $this->handler = $handler;
    }

    public function equalsName(AuthenticationEvent $event): bool
    {
        return $this->name === $event->getName();
    }

    public function equalsHandler(AuthenticationHandler $handler): bool
    {
        return $handler === $this->handler;
    }
}