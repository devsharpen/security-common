<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Event;

class DefaultAuthenticationEvent implements AuthenticationEvent
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var mixed
     */
    protected $target;

    /**
     * @var array|\ArrayAccess
     */
    protected $attributes;

    /**
     * @var bool
     */
    protected $stopPropagation = false;

    /**
     * DefaultAction constructor.
     *
     * @param string     $name
     * @param mixed|null $target
     * @param array|null $params
     */
    public function __construct(string $name, $target = null, array $params = null)
    {
        $this->setName($name);
        $this->setTarget($target);
        $this->setAttributes($params ?? []);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTarget()
    {
        return $this->target;
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function getAttribute(string $key, $default = null)
    {
        if (array_key_exists($key, $this->attributes)) {
            return $this->attributes[$key];
        }

        return $default;
    }

    public function setAttribute(string $key, $value): void
    {
        $this->attributes[$key] = $value;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setTarget($target): void
    {
        $this->target = $target;
    }

    public function setAttributes($attributes): void
    {
        if (!is_array($attributes) && !$attributes instanceof \ArrayAccess) {
            throw new \InvalidArgumentException("Event params are invalid. Expected type is array or \\ArrayAccess. Got " . gettype($attributes));
        }

        $this->attributes = $attributes;
    }

    public function stopPropagation(bool $flag = true): void
    {
        $this->stopPropagation = $flag;
    }

    public function propagationIsStopped(): bool
    {
        return $this->stopPropagation;
    }
}