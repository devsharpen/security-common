<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Event;

interface AuthenticationEmitter
{
    public function newEvent(string $name = null, $target = null, $params = null): AuthenticationEvent;

    public function dispatch(AuthenticationEvent $event): void;

    public function until(AuthenticationEvent $event, callable $callback): void;

    public function attach(string $event, callable $listener, int $priority = 1): AuthenticationHandler;

    public function detach(AuthenticationHandler $handler): bool;
}