<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Messaging;

trait PayloadTrait
{
    /**
     * @var array
     */
    protected $payload;

    /**
     * PayloadTrait constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        $this->init();
        $this->setPayload($payload);
    }

    public function payload(): array
    {
        return $this->payload;
    }

    protected function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }

    abstract protected function init(): void;
}