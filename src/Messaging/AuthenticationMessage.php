<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Messaging;

use Assert\Assertion;
use DateTimeImmutable;
use DateTimeZone;
use Ramsey\Uuid\Uuid;

abstract class AuthenticationMessage implements Message
{
    /**
     * @var string
     */
    protected $messageName;

    /**
     * @var Uuid
     */
    protected $uuid;

    /**
     * @var DateTimeImmutable
     */
    protected $createdAt;

    /**
     * @var array
     */
    protected $metadata = [];

    abstract protected function setPayload(array $payload): void;

    public static function fromArray(array $messageData): AuthenticationMessage
    {
        // MessageDataAssertion::assert($messageData);

        $messageRef = new \ReflectionClass(get_called_class());

        /** @var $message AuthenticationMessage */
        $message = $messageRef->newInstanceWithoutConstructor();
        $message->uuid = Uuid::fromString($messageData['uuid']);
        $message->messageName = $messageData['message_name'];
        $message->metadata = $messageData['metadata'];
        $message->createdAt = $messageData['created_at'];
        $message->setPayload($messageData['payload']);

        return $message;
    }

    protected function init(): void
    {
        if ($this->uuid === null) {
            $this->uuid = Uuid::uuid4();
        }
        if ($this->messageName === null) {
            $this->messageName = get_class($this);
        }
        if ($this->createdAt === null) {
            $this->createdAt = new DateTimeImmutable('now', new DateTimeZone('UTC'));
        }
    }

    public function uuid(): Uuid
    {
        return $this->uuid;
    }

    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function metadata(): array
    {
        return $this->metadata;
    }

    public function toArray(): array
    {
        return [
            'message_name' => $this->messageName,
            'uuid'         => $this->uuid->toString(),
            'payload'      => $this->payload(),
            'metadata'     => $this->metadata,
            'created_at'   => $this->createdAt(),
        ];
    }

    public function messageName(): string
    {
        return $this->messageName;
    }

    public function withMetadata(array $metadata): Message
    {
        $messageData = $this->toArray();
        $messageData['metadata'] = $metadata;

        return static::fromArray($messageData);
    }

    public function withAddedMetadata(string $key, $value): Message
    {
        Assertion::notEmpty($key, 'Invalid key');

        $messageData = $this->toArray();
        $messageData['metadata'][$key] = $value;

        return static::fromArray($messageData);
    }
}