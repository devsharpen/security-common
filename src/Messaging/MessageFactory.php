<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Messaging;

interface MessageFactory
{
    public function fromArray(string $message, array $data): Message;
}