<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Messaging;

interface HasMessageName
{
    public function messageName(): string;
}