<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Messaging;

abstract class TokenEvent extends AuthenticationMessage
{
    public function messageType(): string
    {
        return self::TYPE_EVENT;
    }
}