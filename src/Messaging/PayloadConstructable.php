<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Messaging;

interface PayloadConstructable
{
    public function __construct(array $payload);
}