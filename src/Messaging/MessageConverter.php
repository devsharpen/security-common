<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Messaging;

interface MessageConverter
{
    public function toArray(Message $message): array;
}