<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Messaging;

use Assert\Assertion;

class NoOpMessageConverter implements MessageConverter
{
    public function toArray(Message $message): array
    {
        Assertion::isInstanceOf($message, AuthenticationMessage::class);

        return $message->toArray();
    }
}