<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Messaging;

use DateTimeImmutable;
use Ramsey\Uuid\Uuid;

interface Message
{
    public const TYPE_COMMAND = 'command';
    public const TYPE_EVENT = 'event';
    public const TYPE_RESPONSE = 'response';

    public function messageType(): string;

    public function uuid(): Uuid;

    public function createdAt(): DateTimeImmutable;

    public function payload(): array;

    public function metadata(): array;

    public function withMetadata(array $metadata): Message;

    public function withAddedMetadata(string $key, $value): Message;
}