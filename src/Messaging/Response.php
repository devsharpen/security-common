<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Common\Messaging;

abstract class Response extends AuthenticationMessage
{
    public function messageType(): string
    {
        return self::TYPE_RESPONSE;
    }
}